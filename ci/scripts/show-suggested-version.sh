#!/bin/bash

git_branch=`git branch --show-current`;

version=`xpath -q -e '//project/properties/revision/text()' pom.xml`;
expected_version="${version}.`cut -d "-" -f2 <<< $git_branch`";
if [[ $version != *".$expected_version"* ]] && [ "$git_branch" != "develop" ] && [ "$git_branch" != "master" ]; then
  echo "⚠️ Warning ⚠️"
  echo "Please change the version of your library to ${expected_version} to avoid collisions"
fi
