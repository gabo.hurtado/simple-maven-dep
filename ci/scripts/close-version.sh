#!/bin/bash

git_branch=`git branch --show-current`;

echo $CI_COMMIT_REF_NAME;

if [ -z "$CI_COMMIT_REF_NAME" ]; then
  echo "CI_COMMIT_REF_NAME NULL"
  git_branch=`git branch --show-current`;
else
  echo "CI_COMMIT_REF_NAME NOT NULL"
  git_branch=$CI_COMMIT_REF_NAME;
fi

echo "BRANCH $git_branch"

version=`xpath -q -e '//project/properties/revision/text()' pom.xml`;
expected_version="${version}.`cut -d "-" -f2 <<< $git_branch`";

if [ "$git_branch" == "master" ]; then
  echo "Closing version from ${version} to ${expected_version}..."
  echo "Maven ${MAVEN_HOME}"
  $MAVEN_HOME/bin/mvn $MAVEN_CLI_OPTS build-helper:parse-version versions:set -DnewVersion=${expected_version} versions:commit
fi

xpath -q -e '//project/properties/revision/text()' pom.xml
